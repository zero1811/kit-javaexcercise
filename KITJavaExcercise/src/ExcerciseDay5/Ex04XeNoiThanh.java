package ExcerciseDay5;

import java.util.Scanner;

public class Ex04XeNoiThanh extends Ex04ChuyenXe {
    Scanner scanner = new Scanner(System.in);
    private int soTuyen ;
    private int soKmDiDuoc;
    private int doanhThu;
    public Ex04XeNoiThanh(int maSoChuyen,String hoTenTaXe,int soXe,int soTuyen,int soKmDiDuoc,int doanhThu){
        super(maSoChuyen,hoTenTaXe,soXe);
        this.soTuyen = soTuyen;
        this.soKmDiDuoc = soKmDiDuoc;
        this.doanhThu = doanhThu;
    }

    public int getSoTuyen() {
        return soTuyen;
    }

    public void setSoTuyen(int soTuyen) {
        this.soTuyen = soTuyen;
    }

    public int getSoKmDiDuoc() {
        return soKmDiDuoc;
    }

    public void setSoKmDiDuoc(int soKmDiDuoc) {
        this.soKmDiDuoc = soKmDiDuoc;
    }

    public int getDoanhThu() {
        return doanhThu;
    }

    public void setDoanhThu(int doanhThu) {
        this.doanhThu = doanhThu;
    }

    public Ex04XeNoiThanh() {
    }
    public void input(){
        super.input();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhập số tuyến : ");
        int soTuyen = scanner.nextInt();
        this.setSoTuyen(soTuyen);
        System.out.println("Nhập số km đi được : ");
        int soKm = scanner.nextInt();
        this.setSoKmDiDuoc(soKm);
        System.out.println("Doanh Thu : ");
        int doanThu = scanner.nextInt();
        this.setDoanhThu(doanThu);
    }
}
