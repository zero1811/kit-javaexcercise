package ExcerciseDay5;

import org.omg.Messaging.SYNC_WITH_TRANSPORT;

import java.util.Scanner;

public class Ex01Vehicl {
    private String nameCar;
    private int price; //Giá xe
    private float cylinder; //Dung tích xy lanh

    public String getNameCar() {
        return nameCar;
    }

    public void setNameCar(String nameCar) {
        this.nameCar = nameCar;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public float getCylinder() {
        return cylinder;
    }

    public void setCylinder(float cylinder) {
        this.cylinder = cylinder;
    }

    public Ex01Vehicl(){};
    public Ex01Vehicl(String nameCar,int price,float cylinder){
        this.setNameCar(nameCar);
        this.setPrice(price);
        this.setCylinder(cylinder);
    }
    public Ex01Vehicl(String nameCar,int price){
        this.setNameCar(nameCar);
        this.setPrice(price);
    }
    //Get thuế trước bạ
    public float getRegistrationTax(int price,float cylinder){
        if (cylinder < 100)
            return (float) (price*0.01);
        else if (cylinder > 100 & cylinder < 200)
            return (float) (price*0.03);
        else
            return (float) (price*0.05);
    }
    public void input(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhập thông tin của xe : ");
        System.out.println("Tên xe : ");
        this.setNameCar(scanner.nextLine());
        System.out.println("Nhập giá xe : ");
        this.setPrice(scanner.nextInt());
        System.out.println("Nhập dung tích xy lanh của xe : ");
        this.setCylinder(scanner.nextFloat());

    }
}
