package ExcerciseDay5;

import java.util.Scanner;

public class Ex01main {
    public static void main(String[] args) {
        Ex01Vehicl carOne = new Ex01Vehicl();
        Ex01Vehicl carTwo = new Ex01Vehicl("BMW",10000,650);
        Ex01Vehicl carThree = new Ex01Vehicl("Mercedes C250",20000);
        while (true){
            menu();
            Scanner scanner = new Scanner(System.in);
            int choose = scanner.nextByte();
            switch (choose){
                case 1:{
                    inputData(carOne,carTwo,carThree);
                    break;
                }
                case 2:{
                    printTable(carOne,carTwo,carThree);
                    break;
                }
                case 3:{
                    return;
                }
            }
        }
    }
    public static void menu(){
        System.out.println("Mời bạn chọn : ");
        System.out.println("1. Nhập thông tin");
        System.out.println("2. In bảng tiên thuế trước bạ");
        System.out.println("3. Thoát");
    }
    public static void inputData(Ex01Vehicl carOne,Ex01Vehicl carTwo,Ex01Vehicl carThree){
        carOne.input();

    }
    public static void printTable(Ex01Vehicl carOne,Ex01Vehicl carTwo,Ex01Vehicl carThree){
        System.out.printf("| %20s | %5s | %20s | %20s |\n","Tên Xe","Giá","Dung tích xy lanh","Thuế Trước bạ");
        System.out.printf("| %20s | %5d | %20.2f | %20.0f |\n",carOne.getNameCar(),carOne.getPrice(),carOne.getCylinder(),carOne.getRegistrationTax(carOne.getPrice(),carOne.getCylinder()));
        System.out.printf("| %20s | %5d | %20.2f | %20.0f |\n",carTwo.getNameCar(),carTwo.getPrice(),carTwo.getCylinder(),carTwo.getRegistrationTax(carTwo.getPrice(),carTwo.getCylinder()));
        System.out.printf("| %20s | %5d | %20.2f | %20.0f |\n",carThree.getNameCar(),carThree.getPrice(),carThree.getCylinder(),carThree.getRegistrationTax(carThree.getPrice(),carThree.getCylinder()));
    }
}
