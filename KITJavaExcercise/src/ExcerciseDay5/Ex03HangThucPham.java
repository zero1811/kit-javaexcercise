package ExcerciseDay5;

public class Ex03HangThucPham {
    private int maHang;
    public String tenHang;
    public int donGia;
    private String ngayXanSuat;
    private String ngayHetHan;

    public int getMaHang() {
        return maHang;
    }

    public void setMaHang(int maHang) {
        this.maHang = maHang;
    }

    public String getTenHang() {
        return tenHang;
    }

    public void setTenHang(String tenHang) {
        this.tenHang = tenHang;
    }

    public int getDonGia() {
        return donGia;
    }

    public void setDonGia(int donGia) {
        this.donGia = donGia;
    }

    public String getNgayXanSuat() {
        return ngayXanSuat;
    }

    public void setNgayXanSuat(String ngayXanSuat) {
        this.ngayXanSuat = ngayXanSuat;
    }

    public String getNgayHetHan() {
        return ngayHetHan;
    }

    public void setNgayHetHan(String ngayHetHan) {
        this.ngayHetHan = ngayHetHan;
    }

    public Ex03HangThucPham(int maHang, String tenHang, int donGia, String ngayXanSuat, String ngayHetHan) {
        this.maHang = maHang;
        this.tenHang = tenHang;
        this.donGia = donGia;
        this.ngayXanSuat = ngayXanSuat;
        this.ngayHetHan = ngayHetHan;
    }

    public Ex03HangThucPham(int maHang) {
        this.maHang = maHang;
    }
}
