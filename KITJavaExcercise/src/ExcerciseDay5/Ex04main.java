package ExcerciseDay5;

import java.util.Scanner;

public class Ex04main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Ex04XeNoiThanh[] danhSachNoiThanh = new Ex04XeNoiThanh[100];
        Ex04XeNgoaiThanh[] danhSachNgoaiThanh = new Ex04XeNgoaiThanh[100];
      while (true){
          menu();
          int chon = scanner.nextInt();
          switch (chon){
              case 1:{
                  nhapDanhSach(danhSachNoiThanh,danhSachNgoaiThanh);
                  break;
              }
              case 2:{
                  hienThiDanhSach(danhSachNoiThanh,danhSachNgoaiThanh);
                  break;
              }
              case 3:{
                  return;
              }
          }
      }

    }
    public static void menu(){
        System.out.println("Mời bạn chọn : ");
        System.out.println("1. Nhập danh sách ");
        System.out.println("2. Hiển thị danh sách ");
        System.out.println("3. Doanh Thu");
    }
    public static void nhapDanhSach(Ex04XeNoiThanh[] danhSachNoiThanh,Ex04XeNgoaiThanh[] danhSachNgoaiThanh){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhập danh sách xe nội thành : ");
        for (int i = 0; ; i++) {
            Ex04XeNoiThanh xeNoiThanh = new Ex04XeNoiThanh();
            xeNoiThanh.input();
            danhSachNoiThanh[i] = xeNoiThanh;
            System.out.println("Bạn có muốn nhập tiếp ?(Yes/No)");
            String chon = scanner.nextLine();
            if (chon.equalsIgnoreCase("No"))
                break;

        }
        System.out.println("Nhập danh sách xe ngoại thành : ");
        for (int i = 0;  ; i++) {
            Ex04XeNgoaiThanh xeNgoaiThanh = new Ex04XeNgoaiThanh();
            xeNgoaiThanh.input();
            danhSachNgoaiThanh[i] = xeNgoaiThanh;
            System.out.println("Bạn có muốn nhập tiếp ? (Yes/No)");
            String chon = scanner.nextLine();
            if (chon.equalsIgnoreCase("No"))
                break;
        }
    }
    public static void hienThiDanhSach(Ex04XeNoiThanh[] danhSachNoiThanh,Ex04XeNgoaiThanh[] danhSachNgoaiThanh){
        System.out.println("Danh sách xe nội thành : ");
        System.out.printf("|%15s|%15s|%10s|%10s|%20s|%15s| \n","Mã số chuyến","Họ tên lái xe","Số xe","Số tuyến","Số km đi được","Doanh thu");
        for (int i = 0; i < danhSachNoiThanh.length; i++) {
            System.out.printf("|%15d|%15s|%10d|%10d|%20d|%15d| \n",danhSachNoiThanh[i].maSoChuyen,danhSachNoiThanh[i].hoTenTaiXe,danhSachNoiThanh[i].soXe,danhSachNoiThanh[i].getSoTuyen(),danhSachNoiThanh[i].getSoKmDiDuoc(),danhSachNoiThanh[i].getDoanhThu());
        }
        System.out.println();
        System.out.println("Danh sách xe ngoại thành : ");
        System.out.printf("|%15s|%15s|%10s|%20s|%10s|%15s| \n","Mã số chuyến","Họ tên lái xe","Số xe","Số ngày đi được","Nơi đến","Doanh thu");
        for (int i = 0; i < danhSachNgoaiThanh.length; i++) {
            System.out.printf("|%15d|%15s|%10d|%10d|%20d|%15d| \n",danhSachNgoaiThanh[i].maSoChuyen,danhSachNgoaiThanh[i].hoTenTaiXe,danhSachNgoaiThanh[i].soXe,danhSachNgoaiThanh[i].getSoNgayDiDuoc(),danhSachNgoaiThanh[i].getNoiDen(),danhSachNgoaiThanh[i].getDoanhThu());

        }
    }
    public static void doanhThu(Ex04XeNoiThanh[] danhSachNoiThanh,Ex04XeNgoaiThanh[] danhSachNgoaiThanh){
        int doanhThuNoiThanh = 0;
        int doanhThuNgoaiThanh = 0;
        for (int i = 0; i < danhSachNoiThanh.length; i++) {
            doanhThuNoiThanh += danhSachNoiThanh[i].getDoanhThu();
        }
        for (int i = 0; i < danhSachNoiThanh.length; i++) {
            doanhThuNgoaiThanh += danhSachNgoaiThanh[i].getDoanhThu();
        }
        System.out.println("Doanh thu xe nội thành : "+doanhThuNoiThanh);
        System.out.println("Doanh thu xe ngoại thành : "+doanhThuNgoaiThanh);
    }
}
