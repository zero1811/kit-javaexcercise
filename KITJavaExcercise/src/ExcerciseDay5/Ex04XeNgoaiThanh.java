package ExcerciseDay5;

import java.util.Scanner;

public class Ex04XeNgoaiThanh extends Ex04ChuyenXe{
    Scanner scanner = new Scanner(System.in);
    private String noiDen;
    private int soNgayDiDuoc;
    private int doanhThu;
    public Ex04XeNgoaiThanh(int maSoChuyen, String hoTenTaiXe, int soXe,String noiDen, int soNgayDiDuoc,int doanhThu){
        super(maSoChuyen,hoTenTaiXe,soXe);
        this.noiDen = noiDen;
        this.soNgayDiDuoc = soNgayDiDuoc;
        this.doanhThu = doanhThu;
    }

    public Ex04XeNgoaiThanh() {
    }

    public String getNoiDen() {
        return noiDen;
    }

    public void setNoiDen(String noiDen) {
        this.noiDen = noiDen;
    }

    public int getSoNgayDiDuoc() {
        return soNgayDiDuoc;
    }

    public void setSoNgayDiDuoc(int soNgayDiDuoc) {
        this.soNgayDiDuoc = soNgayDiDuoc;
    }

    public int getDoanhThu() {
        return doanhThu;
    }

    public void setDoanhThu(int doanhThu) {
        this.doanhThu = doanhThu;
    }
    public void input(){
        Scanner scanner = new Scanner(System.in);
        super.input();
        System.out.println("Nhập nơi đến : ");
        String noiDen = scanner.nextLine();
        this.setNoiDen(noiDen);
        System.out.println("Nhập số ngày đi được : ");
        int ngayDi = scanner.nextInt();
        this.setSoNgayDiDuoc(ngayDi);
        System.out.println("Doanh thu : ");
        int doanhThu = scanner.nextInt();
        this.setDoanhThu(doanhThu);
    }
}
